---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://calendar/docs/collection.json)

<!-- END_INFO -->

#01. Выходные дни


<!-- START_00c6530588d4992dc65ab0876209043c -->
## 01.01 Получить список выходных дней

> Example request:

```bash
curl -X GET \
    -G "http://calendar/api/days?year=2021&start=2018&end=2020&holidays=false&timestamps=false" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://calendar/api/days"
);

let params = {
    "year": "2021",
    "start": "2018",
    "end": "2020",
    "holidays": "false",
    "timestamps": "false",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "date": "01.01.2021",
            "timestamp": 1609459200,
            "type": "holiday",
            "comment": "Новогодние каникулы",
            "year": 2021,
            "day": "Friday"
        },
        {
            "date": "02.01.2021",
            "timestamp": 1609545600,
            "type": "holiday",
            "comment": "Новогодние каникулы",
            "year": 2021,
            "day": "Saturday"
        }
    ]
}
```

### HTTP Request
`GET api/days`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `year` |  optional  | Год.
    `start` |  optional  | Первый год (при запросе периода).
    `end` |  optional  | Последний год (при запросе периода).
    `holidays` |  optional  | Получить только список выходных, без сокращённых.
    `timestamps` |  optional  | Получить только список timestamps.

<!-- END_00c6530588d4992dc65ab0876209043c -->


