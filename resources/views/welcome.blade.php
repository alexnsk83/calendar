<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_NAME') }}</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Календарь API</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                </ul>
                <a href="{{ route('login') }}">Вход</a>
            </div>
        </nav>
    </header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <h1>Добро пожаловать</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Этот сервис представляет собой API производственного календаря. С помощью него можно получать список праздничных выходных и сокращённых рабочих дней в формате json-объекта.</p>
                <h3>Примеры запросов</h3>
                <p>{{ request()->url() }}/api/days?year=2021 - для получения выходных за один указанный год</p>
                <p>{{ request()->url() }}/api/days?start=2018&end=2020 - для получения выходных за несколько лет</p>
                <p>{{ request()->url() }}/api/days?year=2021&holidays=true - исключить из выборки сокращённые дни</p>
                <p>{{ request()->url() }}/api/days?year=2021&amp;timestamps=true - получить данные в виде массива timestamp</p>
                <p><a href="{{ request()->url() }}/docs">Документация</a></p>
                <p><a href="{{ route('feedback') }}">Обратная связь</a></p>
            </div>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</html>
