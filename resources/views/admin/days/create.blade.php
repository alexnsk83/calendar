@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-gradient-primary">
                        <div class="card-title">
                            <h3>Новый день</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post">
                            @csrf
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <input type="date" name="first_day" class="form-control" value="{{ old('first_day') }}">
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <input type="date" name="last_day" class="form-control" value="{{ old('last_day') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <select name="type" class="form-control">
                                    <option value="holiday">Выходной</option>
                                    <option value="short">Сокращённый</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" name="comment" class="form-control" placeholder="Комментарий">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Сохранить">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
