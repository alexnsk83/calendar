@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-gradient-primary">
                        <div class="card-tools">
                            <select name="year" class="data-load" data-load="#days">
                                @foreach($years as $year)
                                    <option value="{{ $year->id }}" {{ $year->id === $current_year->id ? 'selected' : '' }}>{{$year->year}}</option>
                                @endforeach
                            </select>
                            <a href="{{ route('day.create') }}" class="btn btn-default">Добавить день</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <th>Дата</th>
                            <th>Timestamp</th>
                            <th>Тип</th>
                            <th>Комментарий</th>
                            <th></th>
                            </thead>
                            <tbody id="days">
                                @foreach($days as $day)
                                    <tr>
                                        <td>{{ $day->date }}</td>
                                        <td>{{ $day->timestamp }}</td>
                                        <td>{{ $day->type == 'short' ? 'Сокращённый' : 'Выходной' }}</td>
                                        <td>{{ $day->comment }}</td>
                                        <td>
                                            <a href="{{ route('day.delete', $day->id) }}"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('body').on('change', '.data-load', function () {
            loadData($(this));
        });
        function loadData (field) {
            $(field.attr('data-load')).css('visibility','hidden');
            $(field.attr('data-load')).empty();
            $(field.attr('data-load')).load('?' + field.attr('name') + '=' + field.val() + ' ' + field.attr('data-load') + ' >*');
            $(field.attr('data-load')).css('visibility','visible');
        }
    </script>
@endsection
