<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/login', 'UserController@login')->name('login');
    Route::post('/login', 'UserController@loginPost')->name('login.post');
});

Route::get('/logout', 'UserController@logout')->name('logout');

Route::group(['prefix' => 'feedback'], function () {
    Route::get('/', 'FeedbackController@index')->name('feedback');
    Route::post('/store', 'FeedbackController@store')->name('feedback.store');
});

Route::group(['middleware' => 'auth', 'prefix' => 'panel'], function () {
    Route::get('/', 'HomeController@panel')->name('admin.home');

    Route::group(['prefix' => 'day'], function () {
        Route::get('/', 'DayController@index')->name('days');
        Route::get('new', 'DayController@create')->name('day.create');
        Route::post('new', 'DayController@store');
        Route::get('{day}', 'DayController@delete')->name('day.delete')->where('day', '[0-9]+');
    });
});
