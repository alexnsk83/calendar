<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackStoreRequest;
use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function index()
    {
        return view('feedback');
    }

    public function store(FeedbackStoreRequest $request)
    {
        Feedback::create($request->all());

        return redirect('/');
    }
}
