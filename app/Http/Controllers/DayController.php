<?php

namespace App\Http\Controllers;

use App\Http\Requests\DayRequest;
use App\Models\Day;
use App\Models\Year;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class DayController extends Controller
{
    public function index(Request $request)
    {
        $years = Year::orderBy('year')->get();
        if (isset($request->year)) {
            $current_year = Year::find($request->year);
        } else {
            $current_year = Year::where('year', date('Y'))->first();
            if (!$current_year) {
                $current_year = Year::create(['year' => date('Y')]);
            }
        }
        $days = Day::where('year_id', $current_year->id)->orderBy('timestamp')->get();

        return view('admin.days.index', compact('years', 'current_year', 'days'));
    }


    public function create()
    {
        return view('admin.days.create');
    }


    public function store(DayRequest $request)
    {
        if (!isset($request->last_day)) {
            $date = Carbon::parse($request->first_day);
            $this->createDay($request, $date);
        } else {
            $period = CarbonPeriod::create(Carbon::parse($request->first_day), Carbon::parse($request->last_day));
            $period = $period->toArray();
            if (count($period) == 0) {
                return redirect()->back()->withErrors('неправильно указан период')->withInput();
            }
            foreach ($period as $date) {
                $this->createDay($request, $date);
            }
        }

        return redirect()->route('days');
    }


    public function delete(Day $day) {
        $day->delete();

        return redirect()->back();
    }


    private function createDay($request, Carbon $date)
    {
        $year = Year::where('year', $date->toDate()->format("Y"))->first();
        if (!$year) {
            $year = Year::create(['year' => $date->toDate()->format("Y")]);
        }
        $day = new Day();
        $day->date = $date->format('d.m.Y');
        $day->timestamp = $date->startOfDay()->timestamp;
        $day->year_id = $year->id;
        $day->type = $request->type;
        $day->comment = $request->comment;
        $day->save();
    }
}
