<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\Day;
use App\Models\Visitor;
use App\Models\Year;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/** @group 01. Выходные дни
 * @package App\Http\Controllers\Api
 */
class CalendarController extends Controller
{
    /**
     * 01.01 Получить список выходных дней
     * @queryParam year Год. Example: 2021
     * @queryParam start Первый год (при запросе периода). Example: 2018
     * @queryParam end Последний год (при запросе периода). Example: 2020
     * @queryParam holidays Получить только список выходных, без сокращённых. Example: false
     * @queryParam timestamps Получить только список timestamps. Example: false
     *
     * @response {
     * "data": [
     * {
     * "date": "01.01.2021",
     * "timestamp": 1609459200,
     * "type": "holiday",
     * "comment": "Новогодние каникулы",
     * "year": 2021,
     * "day": "Friday"
     * },
     * {
     * "date": "02.01.2021",
     * "timestamp": 1609545600,
     * "type": "holiday",
     * "comment": "Новогодние каникулы",
     * "year": 2021,
     * "day": "Saturday"
     * }]}
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $holidays = [];
        if (isset($request->start) && isset($request->end)) {
            if ($request->start >= $request->end) {
                return ResponseHelper::error('Период указан некорректно');
            }
            for ($i = $request->start; $i <= $request->end; $i++) {
                $year = Year::where('year', $i)->first();
                if ($year) {
                    if (isset($request->holidays) && $request->holidays == 'true') {
                        $days = Day::where('year_id', $year->id)->orderBy('timestamp')->holidays()->get();
                    } else {
                        $days = Day::where('year_id', $year->id)->orderBy('timestamp')->get();
                    }
                    $days = $days->toArray();
                    $holidays = array_merge($holidays, $days);
                }
            }
        }
        if (isset($request->year)) {
            $year = Year::where('year', $request->year)->first();
            if ($year) {
                if (isset($request->holidays) && $request->holidays == 'true') {
                    $days = Day::where('year_id', $year->id)->orderBy('timestamp')->holidays()->get();
                } else {
                    $days = Day::where('year_id', $year->id)->orderBy('timestamp')->get();
                }
                $days = $days->toArray();
                $holidays = array_merge($holidays, $days);
            }
        }
        if (isset($request->timestamps) && $request->timestamps == 'true') {
            $timespamps = [];
            foreach ($holidays as $holiday) {
                $timespamps[] = $holiday['timestamp'];
            }
            $holidays = $timespamps;
            $holidays = array_unique($holidays);
        }

        $visitor = Visitor::firstOrCreate(['ip' => $request->ip()]);
        $visitor->visits++;
        $visitor->update();

        return ResponseHelper::success(['data' => $holidays]);
    }
}
