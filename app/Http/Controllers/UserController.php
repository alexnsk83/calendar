<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login()
    {
        return view('admin.login');
    }

    public function loginPost(Request $request)
    {
        $authResult = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ], $request->remember);

        if ($authResult) {
            return redirect()->route('admin.home');
        } else {
            return redirect()->route('login')->withErrors('Неправильный логин или пароль');
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }
}
