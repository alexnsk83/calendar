<?php

namespace App\Helpers;


class ResponseHelper
{
    public static function error($message, $code = 400, $errors = null)
    {
        $code = is_string($code) ? 400 : (int)$code;

        return response()->json(['message' => $message, 'errors' => $errors], $code);
    }

    public static function success($data, $code = 200)
    {
        return response()->json($data, $code);
    }
}
