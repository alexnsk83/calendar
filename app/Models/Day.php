<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    public $timestamps = false;
    protected $appends = ['year', 'day'];
    protected $hidden = ['id', 'year_id'];

    public function year()
    {
        return $this->belongsTo(Year::class);
    }

    public function scopeHolidays($query)
    {
        return $query->where('type', 'holiday');
    }

    public function scopeShort($query)
    {
        return $query->where('type', 'short');
    }

    public function scopeAll($query)
    {
        return $query;
    }

    public function getYearAttribute()
    {
        $year = Year::where('id', $this->year_id)->first();
        return $year->year;
    }

    public function getDayAttribute()
    {
        return Carbon::createFromTimestamp($this->attributes['timestamp'])->getTranslatedDayName();
    }
}
