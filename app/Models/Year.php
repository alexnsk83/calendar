<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $fillable = ['year'];
    public $timestamps = false;


    public function days()
    {
        return $this->hasMany(Day::class);
    }
}
